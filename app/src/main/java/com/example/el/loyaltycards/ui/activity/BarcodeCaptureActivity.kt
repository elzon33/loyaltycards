package com.example.el.loyaltycards.ui.activity

import android.os.Bundle
import com.arellomobile.mvp.MvpAppCompatActivity
import com.arellomobile.mvp.presenter.InjectPresenter
import com.example.el.loyaltycards.App
import com.example.el.loyaltycards.presentation.presenter.BarcodeCapturePresenter
import com.example.el.loyaltycards.presentation.view.BarcodeCaptureView
import me.dm7.barcodescanner.zxing.ZXingScannerView
import ru.terrakok.cicerone.NavigatorHolder
import javax.inject.Inject


class BarcodeCaptureActivity : MvpAppCompatActivity(), BarcodeCaptureView {

    @InjectPresenter
    lateinit var presenter: BarcodeCapturePresenter

    @Inject
    lateinit var navigatorHolder: NavigatorHolder

    @Inject
    lateinit var scannerView: ZXingScannerView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        App.component.inject(this)
        setContentView(scannerView)
    }

    override fun onResume() {
        super.onResume()
        navigatorHolder.setNavigator(navigator)
        sendBarcodeResult()
        scannerView.startCamera()
    }

    override fun onPause() {
        navigatorHolder.removeNavigator()
        super.onPause()
        scannerView.stopCamera()
    }


    fun sendBarcodeResult() {
        scannerView.setResultHandler(ZXingScannerView.ResultHandler { rawResult ->
            presenter.onResult(rawResult)

//            val intent = Intent()
//            intent.putExtra("barcode", rawResult.text)
//            setResult(Activity.RESULT_OK, intent)
//            finish()
        })
    }
}
