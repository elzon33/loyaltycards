package com.example.el.loyaltycards.presentation.presenter

import android.net.Uri
import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.example.el.loyaltycards.presentation.view.ShowImageFullscreenView

@InjectViewState
class ShowImageFullscreenPresenter : MvpPresenter<ShowImageFullscreenView>() {

    private var imageUri: Uri? = null

    fun onCreate(uri: Any?) {
        if (uri != null) {
            imageUri = uri as Uri
            viewState.loadImage(imageUri!!)
        }
    }
}