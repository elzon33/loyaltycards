package com.example.el.loyaltycards.presentation.presenter

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.example.el.loyaltycards.App
import com.example.el.loyaltycards.presentation.view.BarcodeCaptureView
import com.google.zxing.Result
import ru.terrakok.cicerone.Router
import javax.inject.Inject

@InjectViewState
class BarcodeCapturePresenter : MvpPresenter<BarcodeCaptureView>() {

    @Inject
    lateinit var router: Router

    init {
        App.component.inject(this)
    }

    fun onResult(rawResult: Result) {
        router.exitWithResult(BARCODE_CODE_RESULT, rawResult.text)
    }
}