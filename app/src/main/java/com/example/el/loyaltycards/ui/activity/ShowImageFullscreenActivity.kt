package com.example.el.loyaltycards.ui.activity

import android.net.Uri
import android.os.Bundle
import com.arellomobile.mvp.MvpAppCompatActivity
import com.arellomobile.mvp.presenter.InjectPresenter
import com.bumptech.glide.Glide
import com.example.el.loyaltycards.R
import com.example.el.loyaltycards.presentation.presenter.ShowImageFullscreenPresenter
import com.example.el.loyaltycards.presentation.view.ShowImageFullscreenView
import kotlinx.android.synthetic.main.activity_show_image_fullscreen.*

class ShowImageFullscreenActivity : MvpAppCompatActivity(), ShowImageFullscreenView {

    @InjectPresenter
    lateinit var presenter: ShowImageFullscreenPresenter

    // LifeCycle
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_show_image_fullscreen)

        presenter.onCreate(intent.extras.get(URI))
    }

    // ShowImageFullscreenView
    override fun loadImage(imageUri: Uri) {
        Glide.with(this).load(imageUri).into(fullscreenImageView)
    }
}
